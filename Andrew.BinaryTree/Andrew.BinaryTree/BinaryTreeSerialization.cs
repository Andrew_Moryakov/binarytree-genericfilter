﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Andrew.BinaryTree
{
	public static class BinaryTreeSerialization
	{

		public static void SerializeTree<T>(string fileName, Tree<T> serializeObject) where T : IComparable<T>
		{
			///ToDo Сделать обработку недопустимых значений и выбрасывание исключений
			/// ToDo Проверку доступности файла

			IFormatter formatter = new BinaryFormatter();
			using (FileStream s = File.Create(fileName))
					formatter.Serialize(s, serializeObject);
		}

		public static Tree<T> DeserializeTree<T>(string fileName) where T:IComparable<T>
		{
			///ToDo Сделать обработку недопустимых значений и выбрасывание исключений
			///ToDo добавить проверку типов при приведении к объекту.
			/// ToDo существования и проверку доступности файла
			
			IFormatter formatter = new BinaryFormatter();
			Tree<T> deserializeObject = default(Tree<T>);

			using (FileStream s = File.OpenRead(fileName))
			{
				deserializeObject = (Tree<T>)formatter.Deserialize(s);
			}

			return deserializeObject;
		}
	}
}
