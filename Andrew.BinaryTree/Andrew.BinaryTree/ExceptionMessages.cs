﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andrew.BinaryTree
{
	public static class ExceptionMessages
	{
		public const string PathIsNull = "Путь к файлу не указывает на объект.";
		public const string PathIsEmpty = "Путь к файлу не может быть пустым";
		public const string PathTooLong = "Длина файла или директории больше масимально поддерживаемой данной операционной системой.";
		public const string EncodingIsNull = "Кодировка должна указывать на объект";
		public const string TextIsNull = "Текст не должен быть null";
		public const string FileNotExists = "Файл не существует";
		public const string FileNotTxt = "Файл имеет расширение отличное от *.txt";
		public const string FileIsNotRead = "Невозможно прочитать файл";
		public const string FileIsNotWritable = "Невозможно записать текст в файл";
	}
}
