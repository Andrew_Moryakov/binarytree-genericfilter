﻿using System;
using Andrew.BinaryTree.BinaryTree.Interfaces;

namespace Andrew.BinaryTree
{
	//CollectionDataContractAttribute for hashtable or colletcion
	//EnumMemberAttribute for enum member
	//C DataContractAttribute исопльзуй XmlDictionaryReader 

	[Serializable]
	public class Node<T> : INode<T> where T : IComparable<T>
	{
		public T Data { get; }
		public Node<T> Left { get; set; }
		public Node<T> Right { get; set; }
		public Node<T> Parent { get; set; }

		public Node(T data, Node<T> parent )
		{
			Parent = parent;
			Data = data;
		}

		public Node(Node<T> node, Node<T> parent)
		{
				Left = node.Left;
				Right = node.Right;
				Parent = parent;
				Data = node.Data;
		}

		public virtual int CompareTo(T other)
		{
			return Data.CompareTo(other);
		}

		public virtual object Clone()
		{
			return MemberwiseClone();
		}

	}
}
