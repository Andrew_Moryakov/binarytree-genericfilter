﻿using System;

namespace Andrew.BinaryTree.BinaryTree.Interfaces
{
	/// <summary>
	/// Описывает реализацию любого узла, который может использоваться деревом.
	/// </summary>
	/// <typeparam name="T">Тип должен реализовывать IComparable и ICloneable.</typeparam>
	public interface INode<T> : IComparable<T>, ICloneable where T: IComparable<T>
	{
		T Data { get; }
		Node<T> Left { get; set; }
		Node<T> Right { get; set; }
		Node<T> Parent { get; set; }
	}
}
