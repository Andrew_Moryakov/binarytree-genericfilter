﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Andrew.BinaryTree
{
	#region To Do comments
	///ToDo В идеале нужно сделать так, чтобы дерево, само по себе, не умело бы добавлять, удалять, искать узлы. Так как узел, всё знает о своих потомках он и должен заниматься этим, а так-же подсчетом своих подузлов.
	/// ToDo это можно сделать не меняя текущее API дерева.
	#endregion

		///ToDo Дерево не должно работать с сырыми данными, только через INode
	[Serializable]
	public class Tree<T> : IEnumerable<T>, ISerializable where T : IComparable<T>
	{
		private IComparer<T> _comparer;
		private int _count;

		public int Count
		{
			get { return _count; }
		}

		private Node<T> _root;

		public Node<T> Root
		{
			get { return _root; }
			set { _root = value; }
		}

		#region Constructors 
		public Tree()
		{
			Root = null;
		}

		public Tree(Node<T> node)
		{
			ThrowIfNull(node, "Узел не может быть null");

			Root = node;
		}

		public Tree(IEnumerable<T> datas) : this(datas, null)
		{
		}

		public Tree(params T[] datas) : this(datas, null)
		{
		}

		/// <summary>
		/// Создает дерево, которое будет содержать элементы из указанного списка данных, а так-же задает указанный компаратор
		/// </summary>
		/// <param name="nodes">Список объектов узла.</param>
		/// <param name="comparer">Компаратор для сравнения элементов.</param>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="ArgumentException"></exception>
		public Tree(IEnumerable<T> nodes, IComparer<T> comparer)
		{
			ThrowIfNull(nodes, "Список узлов не должен быть null");
			ThrowIfCollectionEmpty(nodes, "Список узлов должен содержать элементы");

			_comparer = comparer;
			foreach (T data in nodes)
			{
				Node<T> node;

				if (TryParseNode(data, out node))
					Root = Add(node, Root, null);

				_count++;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="comparer"></param>
		/// <exception cref="ArgumentNullException"></exception>
		public Tree(IComparer<T> comparer)
		{
			ThrowIfNull(comparer, "Задавая сравниватель, он не должен быть null");

			_comparer = comparer;
		}

		protected Tree(SerializationInfo si, StreamingContext sc)
		{
			_root = (Node<T>) si.GetValue("Root", typeof(Node<T>));
			_count = si.GetInt32("Count");
		}
		#endregion

		/// <summary>
		/// Добавляет даныне в текущее дерево.
		/// </summary>
		/// <param name="data">Данные, которые необходимо добавить в данное дерево.</param>
		/// <exception cref="ArgumentNullException"></exception>
		public void Add(T data)
		{
			ThrowIfNull(data, "Данные не должны быть null");

			Node<T> node;
			if(TryParseNode(data, out node))
			{
				Root = Add(node, Root, null);
				_count++;
			}
		}

		/// <summary>
		/// Удаляет узел с указанными данными из текущего дерева.
		/// </summary>
		/// <param name="data">Данные, которые необходимо удалить из дерева.</param>
		/// <returns>Копия узла, который был удален.</returns>
		/// <exception cref="ArgumentNullException">Если текущее дерево или данные для удаления null</exception>
		public void RemoveNode(T data)
		{
			ThrowIfNull(data, "Данные для удаления не должны быть null");
			ThrowIfNull(Root, "Корень дерево null, невозможно что-либо удалить из пустого дерева");

			Node<T> node;
			if (TryParseNode(data, out node))
			{
				Root = RemoveNode(node, Root);

				if (_count > 0)
					_count--;
			}
		}

		/// <summary>
		/// Производит поиск по текущему дереву возвращает укзел с указанными данными
		/// </summary>
		/// <param name="data">Данные, которые необходимо найти.</param>
		/// <returns>Узел, содержащий найденные данные, если такие данные не найдены, возвращает null.</returns>
		public Node<T> FindNode(T data)
		{
			Node<T> node;

			if (TryParseNode(data, out node))
				return GetFullNodeBackup(node, Root);

			return null;
		}

		public Node<T> GetMax()
		{
			ThrowIfNull(Root, "Корень дерево null");
			return GetMax(Root);
		}

		public Node<T> GetMin()
		{
			ThrowIfNull(Root, "Корень дерево null");
			return GetMin(Root);
		}
		

		private Node<T> GetMax(Node<T> node)
		{
			Node<T> maxNode = node;

			if (maxNode.Right == null)
				return maxNode;

			maxNode = GetMax(maxNode.Right);

			return maxNode;
		}

		private Node<T> GetMin(Node<T> node)
		{
			Node<T> minNode = node;

			if (minNode.Left == null)
				return minNode;

			minNode = GetMin(minNode.Left);

			return minNode;
		}

		/// <summary>
		/// Удаляет узел из дерева.
		/// </summary>
		/// <param name="removedNode">Узел, который необходимо удалить.</param>
		/// <param name="originalRoot">Дерево из которого нужно удалить</param>
		/// <returns>Дерево, из которого удален указанный узел.</returns>
		internal Node<T> RemoveNode(Node<T> removedNode, Node<T> originalRoot)
		{
			if (originalRoot == null)
				return null;

			Node<T> backupSubTree = GetFullNodeBackup(removedNode, originalRoot);
			Node<T> removedTree = RemoveBranch(removedNode, originalRoot);

			if (backupSubTree.Left != null)
				removedTree = Add(backupSubTree.Left, removedTree, null);

			if (backupSubTree.Right != null)
				removedTree = Add(backupSubTree.Right, removedTree, null);

			return removedTree;
		}

		private Node<T> RemoveBranch(Node<T> node, Node<T> root)
		{
			Node<T> cloneRoot = (Node<T>)root.Clone();
			if (root == null)
				return null;

			if (Compare(root.Data, node.Data) == 0)
			{
				if (cloneRoot.Left != null && node.Left != null && (Compare(cloneRoot.Left.Data, cloneRoot.Left.Data)) == 0 &&
					cloneRoot.Right != null && node.Right != null && Compare(cloneRoot.Right.Data, cloneRoot.Right.Data) == 0 &&
					cloneRoot.Parent != null && node.Parent != null && Compare(cloneRoot.Parent.Data, cloneRoot.Parent.Data) == 0)//При возможности ищем максимально точно
				{
					return null;
				}

				return null;
			}

			if (Compare(cloneRoot.Data, node.Data) == 1)
			{
				cloneRoot.Left = RemoveBranch(node, cloneRoot.Left);
			}
			else if (Compare(cloneRoot.Data, node.Data) == -1 || Compare(cloneRoot.Data, node.Data) == 0)
			{
				cloneRoot.Right = RemoveBranch(node, cloneRoot.Right);
			}

			return cloneRoot;
		}

		/// <summary>
		/// Добавляет узел к указанному узлу.
		/// </summary>
		/// <param name="node">Узел, который нужно добавить.</param>
		/// <param name="root">Узел, которому нужно добавить другой узел.</param>
		/// <param name="parent">Узел, который будет являться родителем добавляемого.</param>
		/// <returns>Дерево, представленное узлом к которому добавляли новый узел.</returns>
		private Node<T> Add(Node<T> node, Node<T> root, Node<T> parent)
		{
			if (root == null)
			{
				return new Node<T>(node, parent);
			}

			int compareResult = Compare(root.Data, node.Data);
			if (compareResult < 1)
				root.Right = Add(node, root.Right, root);
			else
				root.Left = Add(node, root.Left, root);

			return root;
		}

		/// <summary>
		/// Получает полную копию узла, со всеми его дочерними узлами
		/// </summary>
		/// <param name="node"></param>
		/// <param name="root"></param>
		/// <returns></returns>
		private Node<T> GetFullNodeBackup(Node<T> node, Node<T> root)
		{
			if (root == null)
				return null;

			if (Compare(root.Data, node.Data) == 0)
			{
				if (root.Left!=null && node.Left!=null && (Compare(root.Left.Data, node.Left.Data)) == 0 &&
				   root.Right != null && node.Right != null && Compare(root.Right.Data, node.Right.Data) == 0 &&
					root.Parent != null && node.Parent != null && Compare(root.Parent.Data, node.Parent.Data) == 0)//При возможности ищем максимально точно
				{
					return root;
				}
				else
				{
					return root;
				}
			}

			if (Compare(root.Data, node.Data) == 1)
				root = GetFullNodeBackup(node, root.Left);
			else
			if (Compare(root.Data, node.Data) == -1 || Compare(root.Data, node.Data) == 0)
				root = GetFullNodeBackup(node, root.Right);

			return root;
		}

		private bool TryParseNode(T data, out Node<T> result )
		{
			if (data == null)
			{
				result = null;
				return false;
			}

			result = new Node<T>(data, null);

			return true;
		}

		private int Compare(T rootData, T data)
		{
			if (_comparer == null)
				return rootData.CompareTo(data);

			return _comparer.Compare(rootData, data);
		}

		private IEnumerator<T> GetDirectEnumarator(Node<T> targetNode)
		{
			var node = targetNode;

			Node<T> leftNode = node;
			while (node != null)
			{
				if (leftNode.Left == null)
				{
					yield return leftNode.Data;
					node = RemoveNode(leftNode, node);
					leftNode = node;
				}
				else
				{
					leftNode = leftNode.Left;
				}
			}
		}

		public IEnumerable<T> GetReversedEnumarator()
		{
			Node<T> node = Root;
			Node<T> rightNode = node;

			while (node != null)
			{
				if (rightNode.Right == null)
				{
					yield return rightNode.Data;
					node = RemoveNode(rightNode, node);
					rightNode = node;
				}
				else
				{
					rightNode = rightNode.Right;
				}
			}
		}

		public IEnumerator<T> GetEnumerator()
		{
			return GetDirectEnumarator(Root);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetDirectEnumarator(Root);
		}

		private void ThrowIfNull(object obj, string mess)
		{
			if (obj==null)
			{
				throw new ArgumentNullException(mess);
			}
		}

		private void ThrowIfCollectionEmpty(IEnumerable<T> collection, string mess)
		{
			if (collection.Count() == 0)
			{
				throw new ArgumentException(mess);
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("Root", _root);
			info.AddValue("Count", _count);
		}

		public bool CompareAllTreeNodes(Tree<T> otherTree)
		{
			if(this._count != otherTree.Count)
				throw new ArgumentException("Размер деревьев должны совпадать.");

			List<T> thisArray = new List<T>();
			List<T> otherArray = new List<T>();

			foreach (T dataNode in this)
			thisArray.Add(dataNode);

			foreach (T dataNode in otherTree)
				otherArray.Add(dataNode);

			for (int i = 0; i < this._count; i++)
			{
				if (thisArray[i].CompareTo(otherArray[i]) != 0)
					return false;
			}

			return true;
		}
	}
}