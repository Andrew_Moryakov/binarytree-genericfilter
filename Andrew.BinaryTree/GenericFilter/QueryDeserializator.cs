﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GenericFilter
{
	internal static class QueryDeserializator
	{
		private static int _functionNamePos = 0;
		private static int _fieldNamePos = 1;
		private static int _paramPos = 2;
		private static char _queryElementsSeparator = ' ';
		private static char _querySeparator = ',';
		private static char _startParamSymbol = '@';

		internal static List<FilterParameters> DeserializationQuery(string queryLine)
		{
			List<string> queryList = new List<string>();
			List<FilterParameters> queryTable = new List<FilterParameters>();

			queryList = queryLine.Split(new[] { _querySeparator }, StringSplitOptions.RemoveEmptyEntries).ToList();

			FilterParameters filterParametrs;
			foreach (string query in queryList)
			{
				string[] queryElements = query.Split(new[] { _queryElementsSeparator }, StringSplitOptions.RemoveEmptyEntries);
				filterParametrs = new FilterParameters();
				filterParametrs.FunctionName = queryElements[_functionNamePos];

				if (queryElements[1][0] != '@')
				filterParametrs.FieldName = queryElements[_fieldNamePos];

				filterParametrs.FunctionParameters = GetParameters(query);

				queryTable.Add(filterParametrs);
			}

			return queryTable;
		}

		internal static string[] GetParameters(string query)
		{
			string parametersPart = new string(query.Skip(query.IndexOf(_startParamSymbol) + 1).ToArray());

			return parametersPart.Split(new[] { _startParamSymbol }, StringSplitOptions.RemoveEmptyEntries);
		}
	}
}
