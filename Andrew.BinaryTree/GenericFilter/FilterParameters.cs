﻿namespace GenericFilter
{
	public class FilterParameters
	{
		private string _functionName;
		private string _fieldName;
		private string[] _functionParameters;

		public FilterParameters()
		{

		}

		public string FunctionName
		{
			get { return _functionName; }
			set { _functionName = value; }
		}

		public string FieldName
		{
			get { return _fieldName; }
			set { _fieldName = value; }
		}

		public string[] FunctionParameters
		{
			get { return _functionParameters; }
			set { _functionParameters = value; }
		}
	}
}
