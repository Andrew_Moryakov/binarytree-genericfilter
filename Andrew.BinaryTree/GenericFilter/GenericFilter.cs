﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using Andrew.BinaryTree;

namespace GenericFilter
{
	public static class GenericFilter<T> where T : IComparable<T>
	{
		private static string _collectionIsNull = "Коллекция не должна быть null.";
		private static string _propertyIsEmpty = "Свойство не может быть пустым.";
		private static string _properyIsNull = "Свойство не может быть null.";
		private static string _parametersIsNull = "Параметры не должны быть null.";
		private static string _parameterIsNull = "Параметр не должнен быть null.";
		private static string _objectIsNull = "Объект не должен быть null.";

		public static IEnumerable<T> Equal(IEnumerable<T> targetCollection, string propertyName, string[] parametrs)
		{
			Validator.ThrowIfObjectNull(targetCollection, _collectionIsNull);
			Validator.ThrowIfEmpty(propertyName, _propertyIsEmpty);
			Validator.ThrowIfStringNull(propertyName, _properyIsNull);
			Validator.ThrowIfObjectNull(parametrs, _parametersIsNull);
			Validator.ThrowIfObjectNull(parametrs[0], _parameterIsNull);
			string otherValue = parametrs[0];

			return targetCollection.Where(item => Equal(item, propertyName, otherValue)).ToList();
		}

		public static bool Equal(T targetObject, string property, string two)
		{
			Validator.ThrowIfObjectNull(targetObject, _objectIsNull);
			Validator.ThrowIfEmpty(property, _propertyIsEmpty);
			Validator.ThrowIfStringNull(property, _properyIsNull);
			Validator.ThrowIfObjectNull(two, _parametersIsNull);

			ConstantExpression constOneValue = GetValuePropertyObject(targetObject, property);
			ConstantExpression constTwoValue = Expression.Constant(Convert.ChangeType(two, constOneValue.Type));

			return Equal(constOneValue.Value, constTwoValue.Value);
		}


		public static bool Equal<K>(K oneValue, K twoValue)
		{
			Validator.ThrowIfObjectNull(oneValue, _objectIsNull);
			Validator.ThrowIfObjectNull(twoValue, _objectIsNull);

			return ExecuteMethod<K, bool>(oneValue, twoValue, GetEqualMethod);
		}

		private static Expression GetEqualMethod<K>(K oneValue, K twoValue)
		{
			ConstantExpression constOneValue = Expression.Constant(oneValue);
			ConstantExpression constTwoValue = Expression.Constant(twoValue);

			return Expression.MakeBinary(ExpressionType.Equal,
				constOneValue, constTwoValue);
		}

		public static IEnumerable<T> Take(IEnumerable<T> targetCollection, string[] parameters)
		{
			string takeNumber = parameters[0];
			Validator.ThrowIfObjectNull(targetCollection, _collectionIsNull);
			Validator.ThrowIfEmpty(takeNumber, _propertyIsEmpty);
			Validator.ThrowIfStringNull(takeNumber, _properyIsNull);

			int takeNumberInt;
			if (!int.TryParse(takeNumber, out takeNumberInt))
			{
				throw new ArgumentException("Параметр должен быть числом", takeNumber);
			}

			return targetCollection.Take(takeNumberInt).ToList();
		}

		private static Expression GetTakeMethod<K>(K oneValue, K twoValue)
		{
			ConstantExpression constOneValue = Expression.Constant(oneValue);
			ConstantExpression constTwoValue = Expression.Constant(twoValue);

			return Expression.MakeBinary(ExpressionType.Equal,
				constOneValue, constTwoValue);
		}

		public static IEnumerable<T> Between(IEnumerable<T> targetCollection, string propertyName, string[] parametrs)
		{
			Validator.ThrowIfObjectNull(targetCollection, _collectionIsNull);
			Validator.ThrowIfEmpty(propertyName, _propertyIsEmpty);
			Validator.ThrowIfStringNull(propertyName, _properyIsNull);
			Validator.ThrowIfObjectNull(parametrs, _parametersIsNull);
			Validator.ThrowIfObjectNull(parametrs[0], _parameterIsNull);

			return (from item in targetCollection
					let min = parametrs[0]
					let max = parametrs[1]
					where Between(item, propertyName, min, max)
					select item).ToList();
		}

		public static bool Between(T targetObject, string property, string min, string max)
		{
			Validator.ThrowIfObjectNull(targetObject, _objectIsNull);
			Validator.ThrowIfEmpty(property, _propertyIsEmpty);
			Validator.ThrowIfStringNull(property, _properyIsNull);
			Validator.ThrowIfObjectNull(min, _parametersIsNull);
			Validator.ThrowIfObjectNull(max, _parametersIsNull);

			ConstantExpression constantPropertyValue = GetValuePropertyObject(targetObject, property);

			object minValue = Convert.ChangeType(min, constantPropertyValue.Type);
			object maxValue = Convert.ChangeType(max, constantPropertyValue.Type);

			return Between(constantPropertyValue.Value, minValue, maxValue);
		}

		public static bool Between<K>(K value, K min, K max)
		{
			Validator.ThrowIfObjectNull(value, _objectIsNull);
			Validator.ThrowIfObjectNull(min, _parametersIsNull);
			Validator.ThrowIfObjectNull(max, _parametersIsNull);

			ParameterExpression valueParam = Expression.Parameter(typeof(K));
			ParameterExpression minParam = Expression.Parameter(typeof(K));
			ParameterExpression maxParam = Expression.Parameter(typeof(K));

			var valueConst = Expression.Constant(value);
			var minConst = Expression.Constant(min);
			var maxConst = Expression.Constant(max);

			BinaryExpression lessThenMax = Expression.MakeBinary(ExpressionType.LessThan,
				valueConst,
				maxConst
				);

			BinaryExpression greaterThenMin = Expression.MakeBinary(ExpressionType.GreaterThan,
				valueConst,
				minConst
				);

			BinaryExpression isBetween = Expression.MakeBinary(ExpressionType.And,
				lessThenMax,
				greaterThenMin);

			var r = Expression.Lambda<Func<K, K, K, bool>>(isBetween, new[] { valueParam, minParam, maxParam });
			return r.Compile()(value, min, max);
		}

		public static IEnumerable<T> Contains(IEnumerable<T> targetCollection, string propertyName, string[] parametrs)
		{
			Validator.ThrowIfObjectNull(targetCollection, _collectionIsNull);
			Validator.ThrowIfEmpty(propertyName, _propertyIsEmpty);
			Validator.ThrowIfStringNull(propertyName, _properyIsNull);
			Validator.ThrowIfObjectNull(parametrs, _parametersIsNull);
			Validator.ThrowIfObjectNull(parametrs[0], _parameterIsNull);
			string subString = parametrs[0];

			return targetCollection.Where(item => Contains(item, propertyName, subString)).ToList();
		}

		public static bool Contains(T targetObject, string property, string subString)
		{
			Validator.ThrowIfObjectNull(targetObject, _objectIsNull);
			Validator.ThrowIfEmpty(property, _propertyIsEmpty);
			Validator.ThrowIfStringNull(property, _properyIsNull);
			Validator.ThrowIfObjectNull(subString, _parametersIsNull);

			ConstantExpression sourceConst = GetValuePropertyObject(targetObject, property);
			ConstantExpression subStringConst = Expression.Constant(subString);

			return Contains(sourceConst.Value, subStringConst.Value);
		}

		public static bool Contains<K>(K source, K subString)
		{
			return ExecuteMethod<K, bool>(source, subString, GetContainsMethod);
		}

		private static Expression GetContainsMethod<K>(K source, K subString)
		{
			ConstantExpression sourceConst = Expression.Constant(source);
			ConstantExpression subStringConst = Expression.Constant(subString);

			string stringValueSource = ToString(source);

			ConstantExpression stringValueConstantSource = Expression.Constant(stringValueSource);
			
			return Expression.Call(
						stringValueConstantSource,
						typeof(string).GetMethod("Contains", new Type[] { typeof(string) }),
						subStringConst
						);
		}

		private static TResult ExecuteMethod<K, TResult>(K oneValue, K twoValue, Func<K, K, Expression> containsMethod)
		{
			ConstantExpression sourceConst = Expression.Constant(oneValue);
			ConstantExpression subStringConst = Expression.Constant(twoValue);

			ParameterExpression sourceParam = Expression.Parameter(sourceConst.Type);
			ParameterExpression subStringParam = Expression.Parameter(subStringConst.Type);

			Type genericLambdaType = Expression.GetDelegateType(sourceConst.Type, subStringConst.Type, typeof(TResult));

			return (TResult)Expression.Lambda(genericLambdaType,
				containsMethod.Invoke(oneValue, twoValue),
				new[] { sourceParam, subStringParam }).Compile().DynamicInvoke(oneValue, twoValue);
		}

		private static ConstantExpression GetValuePropertyObject(T targetObject, string propertyName)
		{
			ParameterExpression objectParam = Expression.Parameter(typeof(T));
			MemberExpression propertyObject = Expression.Property(objectParam, propertyName);

			LambdaExpression lambda = Expression.Lambda(propertyObject, objectParam);
			object propertyValue = lambda.Compile().DynamicInvoke(targetObject);

			return Expression.Constant(propertyValue);
		}

		public static IEnumerable<T> MatchesRegExp(IEnumerable<T> targetCollection, string propertyName, string[] parametrs)
		{
			Validator.ThrowIfObjectNull(targetCollection, _collectionIsNull);
			Validator.ThrowIfEmpty(propertyName, _propertyIsEmpty);
			Validator.ThrowIfStringNull(propertyName, _properyIsNull);
			Validator.ThrowIfObjectNull(parametrs, _parametersIsNull);
			Validator.ThrowIfObjectNull(parametrs[0], _parameterIsNull);

			string regExpString = parametrs[0];

			return targetCollection.Where(item => MatchesRegExp(item, propertyName, regExpString)).ToList();
		}

		public static bool MatchesRegExp(T targetObject, string property, string regExpString)
		{
			Validator.ThrowIfObjectNull(targetObject, _objectIsNull);
			Validator.ThrowIfEmpty(property, _propertyIsEmpty);
			Validator.ThrowIfStringNull(property, _properyIsNull);
			Validator.ThrowIfObjectNull(regExpString, _parametersIsNull);

			ConstantExpression sourceConst = GetValuePropertyObject(targetObject, property);
			ConstantExpression regExpStringConst = Expression.Constant(regExpString);

			return MatchesRegExp(sourceConst.Value, regExpStringConst.Value);
		}

		public static bool MatchesRegExp<T>(T line, T regExpLine)
		{
			Validator.ThrowIfObjectNull(line, _objectIsNull);
			Validator.ThrowIfObjectNull(regExpLine, _objectIsNull);

			return ExecuteMethod<T, bool>(line, regExpLine, GetMatchesRegExpMethod);
		}

		private static Expression GetMatchesRegExpMethod<T>(T value, T regExValue)
		{
			ConstantExpression valueConst = Expression.Constant(value);
			ConstantExpression regExConst = Expression.Constant(regExValue);

			string stringValueSource = ToString(value);

			ConstantExpression stringValueConstantSource = Expression.Constant(stringValueSource);

			Type regexType = typeof(Regex);
			Type[] types = new Type[1];
			types[0] = typeof(string);
			ConstructorInfo constructorInfo = regexType.GetConstructor(types);

			var regExpInstance = Expression.New(constructorInfo, regExConst);

			return Expression.Call(
			regExpInstance,
			typeof(Regex).GetMethod("IsMatch", new Type[] { typeof(String) }), stringValueConstantSource
			);
		}

		private static string ToString<K>(K value)
		{
			ConstantExpression valueConst = Expression.Constant(value);

			MethodCallExpression toStringMethod = Expression.Call(
				valueConst,
				"ToString", null, null
				);

			ParameterExpression sourceParam = Expression.Parameter(valueConst.Type);
			Type toStringLambdaType = Expression.GetDelegateType(valueConst.Type, typeof(string));

			return (string)Expression.Lambda(toStringLambdaType, toStringMethod, new[] { sourceParam }).Compile().DynamicInvoke(valueConst.Value);
		}

		public static IEnumerable<T> ApplyFilter(IEnumerable<T> collection, string queryLine)
		{
			Validator.ThrowIfObjectNull(collection, _collectionIsNull);
			Validator.ThrowIfEmpty(queryLine, _propertyIsEmpty);
			Validator.ThrowIfStringNull(queryLine, _properyIsNull);

			List<FilterParameters> deserializatedQueries = QueryDeserializator.DeserializationQuery(queryLine);
			ParameterExpression methodParametrs = null;
			ParameterExpression collectionParam = null;
			ParameterExpression propertyName = null;

			IEnumerable<T> filteredCollection = collection;
			foreach (FilterParameters query in deserializatedQueries)
			{
				methodParametrs = Expression.Parameter(typeof(string[]));
				collectionParam = Expression.Parameter(typeof(IEnumerable<T>));
				propertyName = Expression.Parameter(typeof(string));

				if (query.FieldName == null)
				{
						var methodCall = Expression.Call(typeof(GenericFilter<T>), query.FunctionName,
							null,
							new[] { collectionParam, methodParametrs });

						filteredCollection = Expression.Lambda<Func<IEnumerable<T>,
							string[],
							IEnumerable<T>>>(methodCall,
							new[] { collectionParam, methodParametrs }).Compile()(filteredCollection, query.FunctionParameters);
				}
				else
				{
					var methodCall = Expression.Call(typeof(GenericFilter<T>),
						query.FunctionName,
						null,
						new[] { collectionParam, propertyName, methodParametrs });

					filteredCollection = Expression.Lambda<Func<IEnumerable<T>,
						string,
						string[],
						IEnumerable<T>>>(methodCall, new[] { collectionParam, propertyName, methodParametrs }).Compile()(filteredCollection, query.FieldName, query.FunctionParameters);
				}
			}

			return filteredCollection;
		}

		public static IEnumerable<T> OrderBy(IEnumerable<T> targetCollection, string propertyName, string[] parametrs = null)
		{
			Validator.ThrowIfObjectNull(targetCollection, _collectionIsNull);
			Validator.ThrowIfEmpty(propertyName, _propertyIsEmpty);
			Validator.ThrowIfStringNull(propertyName, _properyIsNull);

			string methodName = "OrderBy";

			ParameterExpression param = Expression.Parameter(typeof(T));
			Expression propertyExpression = Expression.Property(param, propertyName);

			LambdaExpression resltExpression = Expression.Lambda(propertyExpression, param);
			var lamda = resltExpression.Compile();

			Type enumType = typeof(Enumerable);
			MethodInfo[] methods = enumType.GetMethods(BindingFlags.Public | BindingFlags.Static);
			MethodInfo selectedMethod = methods.First(el => el.Name == methodName && el.GetParameters().Count() == 2);

			MethodInfo method = selectedMethod.MakeGenericMethod(typeof(T), propertyExpression.Type);

			IEnumerable<T> result = (IEnumerable<T>)method.Invoke(null, new object[] { targetCollection, lamda });
			return result.ToList();
		}

		public static IEnumerable<T> OrderByDescending(IEnumerable<T> targetCollection, string propertyName, string[] parametrs = null)
		{
			Validator.ThrowIfObjectNull(targetCollection, _collectionIsNull);
			Validator.ThrowIfEmpty(propertyName, _propertyIsEmpty);
			Validator.ThrowIfStringNull(propertyName, _properyIsNull);

			string methodName = "OrderByDescending";
			ParameterExpression param = Expression.Parameter(typeof(T));
			Expression propertyExpression = Expression.Property(param, propertyName);

			LambdaExpression resltExpression = Expression.Lambda(propertyExpression, param);
			var lamda = resltExpression.Compile();

			Type enumType = typeof(Enumerable);
			MethodInfo[] methods = enumType.GetMethods(BindingFlags.Public | BindingFlags.Static);
			MethodInfo selectedMethod = methods.First(el => el.Name == methodName && el.GetParameters().Count() == 2);

			MethodInfo method = selectedMethod.MakeGenericMethod(typeof(T), propertyExpression.Type);

			IEnumerable<T> result = (IEnumerable<T>)method.Invoke(null, new object[] { targetCollection, lamda });
			return result.ToList();
		}
	}
}