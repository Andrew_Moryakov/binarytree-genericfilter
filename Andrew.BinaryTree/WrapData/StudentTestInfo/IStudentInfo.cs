﻿using System;
using System.Collections.Generic;

namespace Andrew.BinaryTree
{
	/// <summary>
	/// Интерфейс определяет основные требования для класса, который описывает информацию о зданном студентом тесте.
	/// </summary>
	/// <typeparam name="T">Тип, который реализует IComparable, так как любой объект типа, используемый деревом, должен уметь сравнивать себя с другим объектом этого же типа.</typeparam>
	public interface IStudentInfo<T> : IComparable<IStudentInfo<T>> where T : IComparable
	{
		string Name { get; set; }
		string Test { get; set; }
		DateTime Date { get; set; }
		T Score { get; set; }
	}
}