﻿using System;

namespace Andrew.BinaryTree.WrapData.StudentTestInfo
{
	[Serializable]
	public class OtherStudentTestInfo: StudentTestInfoBase<string>, IStudentInfo<string>
	{
		public OtherStudentTestInfo(string name, string test, DateTime date, string score) : base(name, test, date, score)
		{
		}
	}
}
