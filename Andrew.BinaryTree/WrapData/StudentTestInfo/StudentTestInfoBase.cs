﻿using System;
using System.Collections.Generic;

namespace Andrew.BinaryTree
{
	[Serializable]
	public class StudentTestInfoBase<T> : IComparable<IStudentInfo<T>>, IStudentInfo<T>, IComparer<IStudentInfo<T>> where T : IComparable
	{
		private string _name;
		private string _test;
		private DateTime _date;
		private T _score;

		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}
		public string Test
		{
			get { return _test; }
			set { _test = value; }
		}
		public DateTime Date
		{
			get { return _date; }
			set { _date = value; }
		}
		public T Score
		{
			get { return _score; }
			set { _score = value; }
		}

		protected StudentTestInfoBase()
		{
		}

		public StudentTestInfoBase(string name, string test, DateTime date, T score)
		{
			_name = name;
			_test = test;
			_date = date;
			_score = score;
		}

		public int CompareTo(IStudentInfo<T> other)
		{
			//!Notice Equals не пойдет, по тому, что переменные могут иметь одинаковые данные но ссылаться на ранзные объекты
			if (this.Name == other.Name &&
			    this.Date == other.Date &&
			    this.Score.CompareTo(other.Score) == 0 &&
			    this.Test == other.Test)
			{
				return 0;
			}

			if (this.Score.CompareTo(other.Score) == -1)
			{
				return -1;
			}

			if (
			this.Score.CompareTo(other.Score) == 1 )
			{
				return 1;
			}

			return -1;
		}

		public int Compare(IStudentInfo<T> x, IStudentInfo<T> y)
		{
			return x.CompareTo(y);
		}
	}
}