﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Forms;
using Andrew.BinaryTree;
using GenericFilter;

namespace WindowsFormsApplication2
{
	public partial class Form1 : Form
	{
		Tree<StudentTestInfoBase<int>> _tree = new Tree<StudentTestInfoBase<int>>();

		private string selectedCell;
		private string allertSelectedCell = "Прежде чем фильтровать выберите столбец.";
		private string allertEmptyString = "Введите данные.";
		private string allertOnlyNumbers = "Вводите только числа.";
		private string allertBadQuery = "Не корректный запрос.";
		private string allertEmptyQuery = "Введен пустой запрос.";
		private string allertEmptyFilterName = "Введено пустое имя фильтра.";

		private Dictionary<string, string> filters = new Dictionary<string, string>();

		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", new DateTime(2016, 5, 07), 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", new DateTime(2016, 5, 07), 1),
				new StudentTestInfoBase<int>("Filip j. Fry", "Dog test", DateTime.Now.Date, 2),
				new StudentTestInfoBase<int>("Filip j. Fry", "Bending test", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Filip j. Fry", "20th test", DateTime.Now.Date, 2),
				new StudentTestInfoBase<int>("Guenter", "20th test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};

			_tree = new Tree<StudentTestInfoBase<int>>(students);

			AttachTreeToGrid();
		}

		private void buttonAddNewField_Click(object sender, EventArgs e)
		{
			AddNewStudent();
		}

		private void AddNewStudent()
		{
			string studentName = textBoxStudentName.Text;
			string testName = textBoxTestName.Text;

			if (string.IsNullOrEmpty(studentName) && string.IsNullOrEmpty(testName))
				MessageBox.Show(allertEmptyString);

			DateTime testDate = DateTime.ParseExact(textBoxDate.Text, "dd/MM/yyyy", null);

			int score;
			if (!int.TryParse(textBoxScore.Text, out score))
				MessageBox.Show(allertOnlyNumbers);

			_tree.Add(new StudentTestInfoBase<int>(studentName, testName, testDate, score));

			AttachTreeToGrid();
		}

		private void AttachTreeToGrid()
		{
			List<StudentTestInfoBase<int>> listStudentTests = new List<StudentTestInfoBase<int>>();

			dataGridView1.DataSource = _tree.ToList();
		}

		private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
		{
			textBoxDate.Text = monthCalendar1.SelectionStart.ToShortDateString();
		}

		private void textBoxDate_Click(object sender, EventArgs e)
		{
			monthCalendar1.Visible = true;
		}

		private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
		{
			monthCalendar1.Visible = false;
		}

		private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
		{
			for (int i = 0; i < dataGridView1.Columns.Count; i++)
			{
				dataGridView1.Columns[i].DefaultCellStyle.BackColor = Color.White;
				dataGridView1.Columns[i].DefaultCellStyle.ForeColor = Color.Black;
			}

			selectedCell = dataGridView1.Columns[e.ColumnIndex].DataPropertyName;
			dataGridView1.Columns[e.ColumnIndex].DefaultCellStyle.BackColor = Color.DeepSkyBlue;
			dataGridView1.Columns[e.ColumnIndex].DefaultCellStyle.ForeColor = Color.White;

			var sorted = GenericFilter<StudentTestInfoBase<int>>.OrderBy(_tree.ToList(), selectedCell);
			dataGridView1.DataSource = sorted;
		}

		private void textBoxSearcher_TextChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(selectedCell))
			{
				MessageBox.Show(allertSelectedCell);
				textBoxSearcher.Text = "";
				return;
			}

			var sorted = _tree.ToList().Where(el => GenericFilter<StudentTestInfoBase<int>>.Contains(el, selectedCell, textBoxSearcher.Text)); //el.Name.Contains(textBoxSearcher.Text));
			dataGridView1.DataSource = sorted.ToList();
		}

		public bool ContainsInProperty<T>(T instance, string property, string subString)
		{
			ConstantExpression baseObjConst = Expression.Constant(instance);
			MemberExpression propertyOfBaseObj = Expression.Property(baseObjConst, property);
			MethodCallExpression toStringPropertyOfBaseObj = Expression.Call(propertyOfBaseObj, typeof(T).GetMethod("ToString"));
			string paramValueOfInstance = Expression.Lambda<Func<string>>(toStringPropertyOfBaseObj).Compile()();
			ConstantExpression valueOfProperty = Expression.Constant(paramValueOfInstance);

			ConstantExpression subStringConst = Expression.Constant(subString, typeof(string));

			MethodCallExpression containsCall = Expression.Call(valueOfProperty,
				typeof(String).GetMethod("Contains", new Type[] { typeof(string) }),
				subStringConst);

			bool resultExpression = Expression.Lambda<Func<bool>>(containsCall).Compile()();

			return resultExpression;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			ApplyFilter();
		}

		private void ApplyFilter()
		{
			try
			{
				if (!string.IsNullOrEmpty(textBoxFilter.Text))
					dataGridView1.DataSource = GenericFilter<StudentTestInfoBase<int>>.ApplyFilter(_tree.ToList(), textBoxFilter.Text);
				else
					dataGridView1.DataSource = _tree.ToList();
			}
			catch
			{
				MessageBox.Show(allertBadQuery);
			}
		}

		private void buttonAddNewField_Click_1(object sender, EventArgs e)
		{

		}

		private void button2_Click(object sender, EventArgs e)
		{
			SaveFilter();
		}

		private void SaveFilter()
		{
			if (string.IsNullOrEmpty(textBoxFilter.Text) || string.IsNullOrWhiteSpace(textBoxFilter.Text))
				MessageBox.Show(allertEmptyQuery);
			if (string.IsNullOrEmpty(textBoxFilterName.Text) || string.IsNullOrWhiteSpace(textBoxFilterName.Text))
				MessageBox.Show(allertEmptyFilterName);

			filters.Add(textBoxFilterName.Text, textBoxFilter.Text);
			listBox1.Items.Add(textBoxFilterName.Text);
		}

		private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			string selectedQueryName = listBox1.GetItemText(listBox1.SelectedItem);
			string selectedQuery;

			if(filters.TryGetValue(selectedQueryName, out selectedQuery))
			{
				textBoxFilter.Text = selectedQuery;
				ApplyFilter();
			}
		}
	}
}
