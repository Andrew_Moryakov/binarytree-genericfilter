﻿namespace WindowsFormsApplication2
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.textBoxStudentName = new System.Windows.Forms.TextBox();
			this.textBoxTestName = new System.Windows.Forms.TextBox();
			this.textBoxDate = new System.Windows.Forms.TextBox();
			this.textBoxScore = new System.Windows.Forms.TextBox();
			this.buttonAddNewField = new System.Windows.Forms.Button();
			this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
			this.textBoxFilter = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.textBoxSearcher = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.button2 = new System.Windows.Forms.Button();
			this.textBoxFilterName = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView1
			// 
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Location = new System.Drawing.Point(161, 139);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.Size = new System.Drawing.Size(678, 545);
			this.dataGridView1.TabIndex = 0;
			this.dataGridView1.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_ColumnHeaderMouseClick);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 59);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(77, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Имя студента";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 109);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(88, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Название теста";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 167);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(103, 13);
			this.label3.TabIndex = 3;
			this.label3.Text = "Дата прохождения";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 212);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(32, 13);
			this.label4.TabIndex = 4;
			this.label4.Text = "Балл";
			// 
			// textBoxStudentName
			// 
			this.textBoxStudentName.Location = new System.Drawing.Point(15, 75);
			this.textBoxStudentName.Name = "textBoxStudentName";
			this.textBoxStudentName.Size = new System.Drawing.Size(100, 20);
			this.textBoxStudentName.TabIndex = 5;
			this.textBoxStudentName.Text = "Name";
			// 
			// textBoxTestName
			// 
			this.textBoxTestName.Location = new System.Drawing.Point(15, 125);
			this.textBoxTestName.Name = "textBoxTestName";
			this.textBoxTestName.Size = new System.Drawing.Size(100, 20);
			this.textBoxTestName.TabIndex = 6;
			this.textBoxTestName.Text = "Test name";
			// 
			// textBoxDate
			// 
			this.textBoxDate.Location = new System.Drawing.Point(15, 183);
			this.textBoxDate.Name = "textBoxDate";
			this.textBoxDate.ReadOnly = true;
			this.textBoxDate.Size = new System.Drawing.Size(100, 20);
			this.textBoxDate.TabIndex = 7;
			this.textBoxDate.Text = "12.23.2016";
			this.textBoxDate.Click += new System.EventHandler(this.textBoxDate_Click);
			// 
			// textBoxScore
			// 
			this.textBoxScore.Location = new System.Drawing.Point(15, 228);
			this.textBoxScore.Name = "textBoxScore";
			this.textBoxScore.Size = new System.Drawing.Size(100, 20);
			this.textBoxScore.TabIndex = 8;
			this.textBoxScore.Text = "5";
			// 
			// buttonAddNewField
			// 
			this.buttonAddNewField.Location = new System.Drawing.Point(15, 268);
			this.buttonAddNewField.Name = "buttonAddNewField";
			this.buttonAddNewField.Size = new System.Drawing.Size(100, 23);
			this.buttonAddNewField.TabIndex = 9;
			this.buttonAddNewField.Text = "Добавить";
			this.buttonAddNewField.UseVisualStyleBackColor = true;
			this.buttonAddNewField.Click += new System.EventHandler(this.buttonAddNewField_Click);
			// 
			// monthCalendar1
			// 
			this.monthCalendar1.Location = new System.Drawing.Point(127, 183);
			this.monthCalendar1.Name = "monthCalendar1";
			this.monthCalendar1.TabIndex = 10;
			this.monthCalendar1.Visible = false;
			this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
			this.monthCalendar1.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateSelected);
			// 
			// textBoxFilter
			// 
			this.textBoxFilter.Location = new System.Drawing.Point(161, 33);
			this.textBoxFilter.Name = "textBoxFilter";
			this.textBoxFilter.Size = new System.Drawing.Size(535, 20);
			this.textBoxFilter.TabIndex = 11;
			this.textBoxFilter.Text = "OrderBy Score, Between Date @18.05.2016@23.07.2016, Contains Name @Filip, Equal S" +
    "core @1, OrderByDescending Name";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(158, 17);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(161, 13);
			this.label5.TabIndex = 12;
			this.label5.Text = "Напишите запрос фильтрации";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(702, 31);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(137, 23);
			this.button1.TabIndex = 13;
			this.button1.Text = "Применить фильтр";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// textBoxSearcher
			// 
			this.textBoxSearcher.Location = new System.Drawing.Point(161, 113);
			this.textBoxSearcher.Name = "textBoxSearcher";
			this.textBoxSearcher.Size = new System.Drawing.Size(241, 20);
			this.textBoxSearcher.TabIndex = 14;
			this.textBoxSearcher.TextChanged += new System.EventHandler(this.textBoxSearcher_TextChanged);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(158, 97);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(253, 13);
			this.label6.TabIndex = 15;
			this.label6.Text = "Выделите столбец и напишите текст для поиска";
			// 
			// listBox1
			// 
			this.listBox1.FormattingEnabled = true;
			this.listBox1.Location = new System.Drawing.Point(15, 297);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(132, 381);
			this.listBox1.TabIndex = 16;
			this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(304, 54);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(137, 23);
			this.button2.TabIndex = 17;
			this.button2.Text = "Сохранить фильтр";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// textBoxFilterName
			// 
			this.textBoxFilterName.Location = new System.Drawing.Point(161, 56);
			this.textBoxFilterName.Name = "textBoxFilterName";
			this.textBoxFilterName.Size = new System.Drawing.Size(130, 20);
			this.textBoxFilterName.TabIndex = 18;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(847, 693);
			this.Controls.Add(this.textBoxFilterName);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.monthCalendar1);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.textBoxSearcher);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.textBoxFilter);
			this.Controls.Add(this.buttonAddNewField);
			this.Controls.Add(this.textBoxScore);
			this.Controls.Add(this.textBoxDate);
			this.Controls.Add(this.textBoxTestName);
			this.Controls.Add(this.textBoxStudentName);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.dataGridView1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.BindingSource bindingSource1;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBoxStudentName;
		private System.Windows.Forms.TextBox textBoxTestName;
		private System.Windows.Forms.TextBox textBoxDate;
		private System.Windows.Forms.TextBox textBoxScore;
		private System.Windows.Forms.Button buttonAddNewField;
		private System.Windows.Forms.MonthCalendar monthCalendar1;
		private System.Windows.Forms.TextBox textBoxFilter;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBoxSearcher;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox textBoxFilterName;
	}
}

