﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andrew.BinaryTree
{
		public static class Validator
		{
		public static void ThrowIfStringNull(string line, string errMess)
			{
				if (string.IsNullOrEmpty(line))
					throw new ArgumentNullException(errMess);
			}

		public static void ThrowIfEmpty(string line, string errMess)
			{
				if (string.IsNullOrEmpty(line))
					throw new ArgumentException(errMess);
			}

		public static void ThrowIfObjectNull(object obj, string errMess)
			{
				if (obj == null)
					throw new ArgumentNullException(errMess);
			}
	}
}
