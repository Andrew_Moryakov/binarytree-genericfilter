﻿using System;
using System.IO;
using Andrew.BinaryTree;
using Andrew.BinaryTree.WrapData.StudentTestInfo;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Andrew.BinaryTreeTests
{
	[TestClass()]
	public class BinaryTreeSerializationTests
	{
		#region Integer test

		[TestMethod()]
		public void SerializeAsIntegerTreeTest()
		{
			Tree<int> integrTree = new Tree<int>(1, 2, 3, 4, 0);

			string binaryFileName = "SerializeIntegerTreeAsBinary.bin";
			File.Delete(binaryFileName);
			BinaryTreeSerialization.SerializeTree<int>(binaryFileName, integrTree);

			Assert.IsTrue(File.Exists(binaryFileName));
		}

		[TestMethod()]
		public void DeserializeAsIntegerTreeTest()
		{
			Tree<int> integrTree = new Tree<int>(1, 2, 3, 4, 0);

			string binaryFileName = "SerializeIntegerTreeAsBinary.bin";
			File.Delete(binaryFileName);
			BinaryTreeSerialization.SerializeTree<int>(binaryFileName, integrTree);
			Tree<int> des = (Tree<int>) BinaryTreeSerialization.DeserializeTree<int>(binaryFileName);

			Assert.IsTrue(File.Exists(binaryFileName));
			Assert.IsTrue(integrTree.CompareAllTreeNodes(des));
		}

		[TestMethod()]
		public void SerializeAsEmptyIntegerTreeTest()
		{
			Tree<int> integrTree = new Tree<int>();

			string binaryFileName = "SerializeIntegerTreeAsBinary.bin";
			File.Delete(binaryFileName);
			BinaryTreeSerialization.SerializeTree<int>(binaryFileName, integrTree);

			Assert.IsTrue(File.Exists(binaryFileName));
		}

		[TestMethod()]
		public void DeserializeAsEmptyIntegerTreeTest()
		{
			Tree<int> integrTree = new Tree<int>();

			string binaryFileName = "SerializeIntegerTreeAsBinary.bin";
			File.Delete(binaryFileName);
			BinaryTreeSerialization.SerializeTree<int>(binaryFileName, integrTree);
			Tree<int> des = (Tree<int>) BinaryTreeSerialization.DeserializeTree<int>(binaryFileName);

			Assert.IsTrue(File.Exists(binaryFileName));
			Assert.IsTrue(integrTree.CompareAllTreeNodes(des));
		}

		#endregion

		#region Student info test

		[TestMethod()]
		public void SerializeAsEmptyStudentInfoTestTreeTest()
		{

			Tree<OtherStudentTestInfo> studentTestInfoTree = new Tree<OtherStudentTestInfo>(
				new OtherStudentTestInfo("Name 1", "Test 2", DateTime.Now, "A1"),
				new OtherStudentTestInfo("Name 3", "Test 2", DateTime.Now, "A2"),
				new OtherStudentTestInfo("Name 2", "Test 1", DateTime.Now, "A1+"),
				new OtherStudentTestInfo("Name 7", "Test 2", DateTime.Now, "B2")
				);

			string binaryFileName = "SerializeStudTestInfTreeAsBinary.bin";
			File.Delete(binaryFileName);
			BinaryTreeSerialization.SerializeTree<OtherStudentTestInfo>(binaryFileName, studentTestInfoTree);

			Assert.IsTrue(File.Exists(binaryFileName));
		}

		[TestMethod()]
		public void DeserializeAsEmptyStudentInfoTestTreeTest()
		{
			Tree<OtherStudentTestInfo> studentTestInfoTree = new Tree<OtherStudentTestInfo>(
				new OtherStudentTestInfo("Name 1", "Test 2", DateTime.Now, "A1"),
				new OtherStudentTestInfo("Name 3", "Test 2", System.DateTime.Now, "A2"),
				new OtherStudentTestInfo("Name 2", "Test 1", DateTime.Now, "A1+"),
				new OtherStudentTestInfo("Name 7", "Test 2", DateTime.Now, "B2")
				);

			string binaryFileName = "SerializeStudTestInfTreeAsBinary.bin";
			File.Delete(binaryFileName);
			BinaryTreeSerialization.SerializeTree<OtherStudentTestInfo>(binaryFileName, studentTestInfoTree);
			Tree<OtherStudentTestInfo> des = BinaryTreeSerialization.DeserializeTree<OtherStudentTestInfo>(binaryFileName);

			Assert.IsTrue(File.Exists(binaryFileName));
			Assert.IsTrue(studentTestInfoTree.CompareAllTreeNodes(des));
		}

		[TestMethod()]
		public void SerializeAsStudentInfoTestTreeTest()
		{

			Tree<OtherStudentTestInfo> studentTestInfoTree = new Tree<OtherStudentTestInfo>(
				new OtherStudentTestInfo("Name 1", "Test 2", DateTime.Now, "A1"),
				new OtherStudentTestInfo("Name 3", "Test 2", DateTime.Now, "A2"),
				new OtherStudentTestInfo("Name 2", "Test 1", DateTime.Now, "A1+"),
				new OtherStudentTestInfo("Name 7", "Test 2", DateTime.Now, "B2")
				);

			string binaryFileName = "SerializeStudTestInfTreeAsBinary.bin";
			File.Delete(binaryFileName);
			BinaryTreeSerialization.SerializeTree<OtherStudentTestInfo>(binaryFileName, studentTestInfoTree);

			Assert.IsTrue(File.Exists(binaryFileName));
		}

		[TestMethod()]
		public void DeserializeAsStudentInfoTestTreeTest()
		{
			Tree<OtherStudentTestInfo> studentTestInfoTree = new Tree<OtherStudentTestInfo>(
				new OtherStudentTestInfo("Name 1", "Test 2", DateTime.Now, "A1"),
				new OtherStudentTestInfo("Name 3", "Test 2", DateTime.Now, "A2"),
				new OtherStudentTestInfo("Name 2", "Test 1", DateTime.Now, "A1+"),
				new OtherStudentTestInfo("Name 7", "Test 2", DateTime.Now, "B2")
				);

			string binaryFileName = "SerializeStudTestInfTreeAsBinary.bin";
			File.Delete(binaryFileName);
			BinaryTreeSerialization.SerializeTree<OtherStudentTestInfo>(binaryFileName, studentTestInfoTree);
			Tree<OtherStudentTestInfo> des = BinaryTreeSerialization.DeserializeTree<OtherStudentTestInfo>(binaryFileName);

			Assert.IsTrue(File.Exists(binaryFileName));
			Assert.IsTrue(studentTestInfoTree.CompareAllTreeNodes(des));
		}

		#endregion

		#region String test

		[TestMethod()]
		public void SerializeAsEmptyStringTreeTest()
		{
			Tree<string> stringTree = new Tree<string>();

			string binaryFileName = "SerializeEmptyStringTreeAsBinary.bin";
			File.Delete(binaryFileName);
			BinaryTreeSerialization.SerializeTree<string>(binaryFileName, stringTree);

			Assert.IsTrue(File.Exists(binaryFileName));
		}

		[TestMethod()]
		public void DeserializeAsEmptyStringTreeTest()
		{
			Tree<string> stringTree = new Tree<string>();

			string binaryFileName = "SerializeEmptyStringTreeAsBinary.bin";
			File.Delete(binaryFileName);
			BinaryTreeSerialization.SerializeTree<string>(binaryFileName, stringTree);
			Tree<string> des = BinaryTreeSerialization.DeserializeTree<string>(binaryFileName);

			Assert.IsTrue(File.Exists(binaryFileName));
			Assert.IsTrue(stringTree.CompareAllTreeNodes(des));
		}

		[TestMethod()]
		public void SerializeAsStringTreeTest()
		{
			Tree<string> stringTree = new Tree<string>("A", "B", "C", "D", "1");

			string binaryFileName = "SerializeStringTreeAsBinary.bin";
			File.Delete(binaryFileName);
			BinaryTreeSerialization.SerializeTree<string>(binaryFileName, stringTree);

			Assert.IsTrue(File.Exists(binaryFileName));
		}

		[TestMethod()]
		public void DeserializeAsStringTreeTest()
		{
			Tree<string> stringTree = new Tree<string>("A", "B", "C", "D", "1");

			string binaryFileName = "SerializeStringTreeAsBinary.bin";
			File.Delete(binaryFileName);
			BinaryTreeSerialization.SerializeTree<string>(binaryFileName, stringTree);
			Tree<string> des = BinaryTreeSerialization.DeserializeTree<string>(binaryFileName);

			Assert.IsTrue(File.Exists(binaryFileName));
			Assert.IsTrue(stringTree.CompareAllTreeNodes(des));
		}

		#endregion

	}
}