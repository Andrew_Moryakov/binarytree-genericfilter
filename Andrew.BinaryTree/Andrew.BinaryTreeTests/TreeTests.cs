﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Andrew.BinaryTree.BinaryTree;
using GenericFilter;

namespace Andrew.BinaryTree.Tests
{
	[TestClass()]
	public class TreeTests
	{
		#region Integer Tree

		#region others

		[TestMethod()]
		public void CreateEmptyTree()
		{
			Tree<int> tree = new Tree<int>();

			Assert.IsNull(tree.Root);
		}

		[TestMethod()]
		public void CreateTreeWithOneNode()
		{
			int expected = 1;
			Tree<int> tree = new Tree<int>();
			tree.Add(expected);


			Assert.IsNotNull(tree.Root);
			Assert.AreEqual(expected, tree.Root.Data);
		}

		[TestMethod()]
		public void GetMaxMinWhenAllZiro()
		{
			Tree<int> tree = new Tree<int>(new[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});

			Assert.AreEqual(0, tree.GetMax().Data);
			Assert.AreEqual(0, tree.GetMin().Data);
		}

		[TestMethod()]
		public void RemoveWhenAllZiro()
		{
			Tree<int> tree = new Tree<int>(new[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});

			int beforeRemoveLength = tree.Count;

			tree.RemoveNode(0);

			int afterRemoveLength = tree.Count + 1;

			Assert.AreEqual(beforeRemoveLength, afterRemoveLength);
		}

		[TestMethod()]
		public void AddToEmptyTree()
		{
			Tree<int> tree = new Tree<int>();

			tree.Add(1);

			Assert.AreEqual(1, tree.Count);
			Assert.AreEqual(1, tree.Root.Data);
		}

		#endregion

		#region Two integer nodes

		[TestMethod()]
		public void AddToRightElementTree()
		{
			int expected = 2;
			int root = 1;
			Tree<int> tree = new Tree<int>();
			tree.Add(root);

			tree.Add(expected);

			Assert.AreEqual(expected, tree.Root.Right.Data);
		}

		[TestMethod()]
		public void AddToLeftElementTree()
		{
			int expected = 0;
			int root = 1;
			Tree<int> tree = new Tree<int>();
			tree.Add(root);

			tree.Add(expected);

			Assert.IsNotNull(tree.Root);
			Assert.AreEqual(expected, tree.Root.Left.Data);
		}

		[TestMethod()]
		public void AddTwoRightElementsToTree()
		{
			int root = 1;
			int oneRight = 2;
			int twoRight = 3;
			Tree<int> tree = new Tree<int>();
			tree.Add(root);

			tree.Add(oneRight);
			tree.Add(twoRight);

			Assert.AreEqual(oneRight, tree.Root.Right.Data);
			Assert.AreEqual(twoRight, tree.Root.Right.Right.Data);
		}

		[TestMethod()]
		public void AddTwoLeftElementsToTree()
		{
			int root = 4;
			int oneLeft = 2;
			int twoLeft = 3;
			Tree<int> tree = new Tree<int>();
			tree.Add(root);

			tree.Add(oneLeft);
			tree.Add(twoLeft);

			Assert.AreEqual(oneLeft, tree.Root.Left.Data);
			Assert.AreEqual(twoLeft, tree.Root.Left.Right.Data);
		}

		[TestMethod()]
		public void AddToLeftAndRightElementTree()
		{
			int expectedLeft = 0;
			int expectedRight = 2;
			int root = 1;
			Tree<int> tree = new Tree<int>();
			tree.Add(root);

			tree.Add(expectedLeft);
			tree.Add(expectedRight);

			Assert.IsNotNull(tree.Root);
			Assert.AreEqual(expectedLeft, tree.Root.Left.Data);
			Assert.AreEqual(expectedRight, tree.Root.Right.Data);
		}

		#endregion

		#region Min Max

		[TestMethod()]
		public void GetMaxNode()
		{
			Tree<int> tree = new Tree<int>(new[] {3, 1, 2, 1, 7, 4, 3, 4});

			Node<int> actual = tree.GetMax();

			Assert.AreEqual(7, actual.Data);
		}

		[TestMethod()]
		public void GetMinNode()
		{
			Tree<int> tree = new Tree<int>(new[] {3, 1, 2, 1, 7, 4, 3, 4});

			Node<int> actual = tree.GetMin();

			Assert.AreEqual(1, actual.Data);
		}

		#endregion

		#region Enumarator tree

		[TestMethod()]
		public void EnumerateIntegerTree()
		{
			Tree<int> tree =
				new Tree<int>(new[]
				{8, 1, 4, 0, 1, 2, 1, 3, 2, 4, 5, 6, 6, 7, 5, 8, 1, 0, 2, 3, 4, 3, 7, 1, 3, 3, 2, 35, 3, 4, 7, 8, 0, 56});

			List<int> rs = new List<int>(8);
			foreach (var item in tree)
				rs.Add(item);

			Assert.AreEqual(tree.Count, rs.Count);
			Assert.IsTrue(rs[0] < rs[rs.Count - 1]);
		}

		[TestMethod()]
		public void GetReversComparerTree()
		{
			Tree<int> tree =
				new Tree<int>(new[]
				{8, 1, 4, 0, 1, 2, 1, 3, 2, 4, 5, 6, 6, 7, 5, 8, 1, 0, 2, 3, 4, 3, 7, 1, 3, 3, 2, 35, 3, 4, 7, 8, 0, 56});

			List<int> rs = new List<int>(8);
			foreach (var item in tree.GetReversedEnumarator())
				rs.Add(item);

			Assert.AreEqual(tree.Count, rs.Count);
			Assert.IsTrue(rs[0] > rs[rs.Count - 1]);
		}

		#endregion

		#region Remove 

		[TestMethod()]
		public void RemoveIntegerNumberFromTree()
		{
			Tree<int> tree = new Tree<int>(new[] {3, 1, 2, 1, 7, 4, 3, 4});

			int beforeRemoveLength = tree.Count;

			int actual = 7;
			tree.RemoveNode(actual);

			int afterRemoveLength = tree.Count + 1;

			Node<int> removedNode = tree.FindNode(actual);

			Assert.IsNull(removedNode);
			Assert.AreEqual(beforeRemoveLength, afterRemoveLength);
		}

		[TestMethod()]
		public void RemoveRightNodeTree()
		{
			Tree<int> tree = new Tree<int>(new[] {3, 1, 2, 1, 7, 4, 3, 4});

			int beforeRemoveLength = tree.Count;

			int actual = 7;
			tree.RemoveNode(actual);

			int afterRemoveLength = tree.Count + 1;

			Assert.AreNotEqual(7, tree.Root.Right.Data);
			Assert.AreEqual(beforeRemoveLength, afterRemoveLength);
		}

		[TestMethod()]
		public void RemoveLeftNodeTree()
		{
			Tree<int> tree = new Tree<int>(new[] {3, 1, 2, 1, 7, 4, 3, 4});

			int beforeRemoveLength = tree.Count;

			int actual = 1;
			tree.RemoveNode(actual);

			int afterRemoveLength = tree.Count;

			Assert.AreNotEqual(1, tree.Root.Left.Data);
			Assert.AreNotEqual(beforeRemoveLength, afterRemoveLength);
		}

		[TestMethod()]
		public void RemoveRootNodeTree()
		{
			Tree<int> tree = new Tree<int>(new[] {3, 1, 2, 1, 7, 4, 3, 4});

			int beforeRemoveLength = tree.Count;

			int actual = 3;
			tree.RemoveNode(actual);

			int afterRemoveLength = tree.Count;

			Assert.AreNotEqual(beforeRemoveLength, afterRemoveLength);
			Assert.AreEqual(1, tree.Root.Data);
		}

		[TestMethod()]
		public void RemoveAllNumbersFromTree()
		{
			Tree<int> tree = new Tree<int>(new[] {3, 1});

			tree.RemoveNode(3);
			tree.RemoveNode(1);

			Assert.IsNull(tree.Root);
		}

		#endregion

		#endregion

		#region String tree

		[TestMethod()]
		public void AddStringListToTree()
		{
			IEnumerable<string> strings = new List<string>
			{
				"Один",
				"Два",
				"три",
				"234234324215443253",
				"1",
				"Two",
				"Three",
				"Four",
				"Five",
				"six",
				""
			};
			Tree<string> tree = new Tree<string>(strings);

			Assert.AreEqual(11, tree.Count);
		}

		[TestMethod()]
		public void AppendStringListToTree()
		{
			IEnumerable<string> strings = new List<string>
			{
				"Один",
				"Два",
				"три",
				"234234324215443253",
				"1",
				"Two",
				"Three",
				"Four",
				"Five",
				"six",
				""
			};
			Tree<string> tree = new Tree<string>(strings);
			tree.Add("'''''''");

			Assert.AreEqual(12, tree.Count);
		}

		[TestMethod()]
		public void EnumerateReverseForString()
		{
			IEnumerable<string> strings = new List<string>
			{
				"Один",
				"Два",
				"три",
				"234234324215443253",
				"1",
				"Two",
				"Three",
				"Four",
				"Five",
				"six",
				""
			};
			Tree<string> tree = new Tree<string>(strings);
			tree.Add("'''''''");

			List<string> stringList = new List<string>();

			foreach (var item in tree.GetReversedEnumarator())
				stringList.Add(item);

			Assert.AreEqual(12, tree.Count);
			Assert.IsTrue(stringList[0].CompareTo(stringList[stringList.Count - 1]) == 1);
		}

		[TestMethod()]
		public void EnumerateForString()
		{
			IEnumerable<string> strings = new List<string>
			{
				"Один",
				"Два",
				"три",
				"234234324215443253",
				"1",
				"Two",
				"Three",
				"Four",
				"Five",
				"six",
				""
			};
			Tree<string> tree = new Tree<string>(strings);
			tree.Add("'''''''");

			List<string> stringList = new List<string>();

			foreach (var item in tree)
				stringList.Add(item);

			Assert.AreEqual(12, tree.Count);
			Assert.IsTrue(stringList[0].CompareTo(stringList[stringList.Count - 1]) == -1);
		}

		[TestMethod()]
		public void RemoveString()
		{
			IEnumerable<string> strings = new List<string>
			{
				"Один",
				"Два",
				"три",
				"234234324215443253",
				"1",
				"Two",
				"Three",
				"Four",
				"Five",
				"six",
				""
			};
			Tree<string> tree = new Tree<string>(strings);

			tree.RemoveNode("234234324215443253");

			Node<string> finded = tree.FindNode("234234324215443253");

			Assert.IsNull(finded);
		}

		#endregion

		#region Students test info

		[TestMethod()]
		public void CreateStudentsTestInfoTree()
		{
			StudentTestInfoBase<int> studentTest = new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 6);

			List<StudentTestInfoBase<int>> students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 5),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 4),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 4),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 3)
			};
			
			Tree<StudentTestInfoBase<int>> studentsTree = new Tree<StudentTestInfoBase<int>>(students, studentTest);

			Assert.IsNotNull(studentsTree);
			Assert.AreEqual(studentsTree.Count, 4);
		}

		[TestMethod()]
		public void AddNewStudentsTestInfoToTree()
		{
			StudentTestInfoBase<int> studentTest = new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 6);

			List<StudentTestInfoBase<int>> students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 5),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 4),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 4),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 3)
			};

			students.Add(studentTest);

			Tree<StudentTestInfoBase<int>> studentsTree = new Tree<StudentTestInfoBase<int>>(students, studentTest);

			Assert.IsNotNull(studentsTree);
			Assert.AreEqual(studentsTree.Count, 5);
		}

		#region Min Max

		[TestMethod()]
		public void GetMaxSudentsTestInfoNode()
		{
			StudentTestInfoBase<int> studentTest = new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 6);

			List<StudentTestInfoBase<int>> students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 5),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 4),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 4),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 3),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 7)
			};

			Tree<StudentTestInfoBase<int>> studentsTree = new Tree<StudentTestInfoBase<int>>(students, studentTest);

			var max = studentsTree.GetMax();
		}

		[TestMethod()]
		public void GetMinSudentsTestInfo()
		{
			Tree<int> tree = new Tree<int>(new[] {3, 1, 2, 1, 7, 4, 3, 4});

			Node<int> actual = tree.GetMin();

			Assert.AreEqual(1, actual.Data);
		}

		#endregion

		#region Stedent enumerate

		[TestMethod()]
		public void EnumerateStudentInfoTestTree()
		{
			StudentTestInfoBase<int> studentTest = new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 6);

			List<StudentTestInfoBase<int>> students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 5),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 4),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 4),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 3),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 7)
			};

			Tree<StudentTestInfoBase<int>> studentsTree = new Tree<StudentTestInfoBase<int>>(students, studentTest);

			List<int> scores = new List<int>();
			foreach (var item in studentsTree)
			{
				scores.Add(item.Score);
			}

			Assert.IsTrue(scores[0] < scores[scores.Count - 1]);
		}

		[TestMethod()]
		public void EnumerateReverseStudentInfoTestTree()
		{
			StudentTestInfoBase<int> studentTest = new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 6);

			List<StudentTestInfoBase<int>> students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 5),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 4),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 4),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 3),
				new StudentTestInfoBase<int>("name", "Test", DateTime.Now, 7)
			};

			Tree<StudentTestInfoBase<int>> studentsTree = new Tree<StudentTestInfoBase<int>>(students, studentTest);

			List<int> scores = new List<int>();
			foreach (var item in studentsTree.GetReversedEnumarator())
			{
				scores.Add(item.Score);
			}

			Assert.IsTrue(scores[0] > scores[scores.Count - 1]);
		}

		#endregion

		#region Expression test
		[TestMethod]
		public void ExpressionFilteringByDateTreeTest()
		{
			DateTime doDateTime = DateTime.Now;
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "Mathematics of quantum neutrino fields", doDateTime, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", new DateTime(2015, 9, 12), 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning test", doDateTime, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", doDateTime, 1),
				new StudentTestInfoBase<int>("Seymour", "Mathematics of quantum neutrino fields", doDateTime, 0)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Where(el => GenericFilter<DateTime>.Equal(el.Date, DateTime.Now.Date)).ToList();

			Assert.IsTrue(filtered.All(el => el.Date == doDateTime));
		}

		[TestMethod]
		public void ExpressionFilteringByScoreTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 0)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Where(el => GenericFilter<int>.Equal(el.Score, 6)).ToList();

			Assert.IsTrue(filtered.All(el=>el.Score == 6));
		}


		[TestMethod]
		public void ExpressionFilteringByRegexTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Where(el => GenericFilter<string>.MatchesRegExp(el.Test, @"\[\<.{1,50}\>\].{1,50}\[\<[1-3]00\>\]")).ToList();

			Assert.IsTrue(filtered.Count() == 1);
		}


		[TestMethod]
		public void ExpressionSortingToUpTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.OrderBy(el => el.Name).ToList();

			Assert.IsTrue(filtered[0].Score > filtered[filtered.Count() - 1].Score);
		}


		[TestMethod]
		public void ExpressionSortingToDownTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.OrderByDescending(el => el.Name).ToList();

			Assert.IsTrue(filtered[0].Score < filtered[filtered.Count() - 1].Score);
		}


		[TestMethod]
		public void ExpressionSkipTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Skip(2);

			Assert.IsTrue(filtered.Count() == 3);
		}

		[TestMethod]
		public void ExpressionAllTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.All(el => GenericFilter<int>.Equal(el.Score, 5));

			Assert.IsFalse(filtered);
		}

		[TestMethod]
		public void ExpressionAnyTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Any(el => GenericFilter<int>.Equal(el.Score, 5));

			Assert.IsTrue(filtered);
		}

		[TestMethod]
		public void ExpressionAverageTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Average(el=>el.Score);

			int aver = 0;

			foreach (var studentTest in studentsTree)
			{
				aver += studentTest.Score;
			}
			aver /= studentsTree.Count;

			Assert.AreEqual(filtered, aver);
		}

		[TestMethod]
		public void ExpressionCountTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Count();

			Assert.IsTrue(filtered == 5);
		}

		[TestMethod]
		public void ExpressionContainsTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Where(el => GenericFilter<string>.Contains(el.Test, "Math"));

			Assert.IsTrue(filtered.Count() == 2);
		}

		[TestMethod]
		public void ExpressionFirstTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.First();

			Assert.AreEqual(filtered.Name, "Filip j. Fry");
		}

		[TestMethod]
		public void ExpressionLastTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Last();

			Assert.AreEqual(filtered.Name, "Hubert J. Farnsworth");
		}

		[TestMethod]
		public void ExpressionMinTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Min(el=>el.Score);

			Assert.AreEqual(filtered, 1);
		}

		[TestMethod]
		public void ExpressionMaxTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Max(el=>el.Score);

			Assert.AreEqual(filtered, 6);
		}

		[TestMethod]
		public void ExpressionToArrayTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.ToArray();

			Assert.IsTrue(filtered is Array);
		}

		[TestMethod]
		public void ExpressionToDictionaryTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

				var filtered = studentsTree.ToDictionary(p => p.Name, p => p.Score);

			Assert.IsTrue(filtered is Dictionary<string, int>);
			Assert.AreEqual(1, filtered.Values.First());
		}

		[TestMethod]
		public void ExpressionToListTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.ToList();

			Assert.IsTrue(filtered is List<StudentTestInfoBase<int>>);
			Assert.AreEqual(1, filtered.First().Score);
		}

		[TestMethod]
		public void ExpressionGetAllTestsTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Select(el => el.Test);

			Assert.IsTrue(filtered is IEnumerable<string>);
			Assert.AreEqual(filtered.ElementAt(0), "Mathematics of quantum neutrino fields");
		}

		[TestMethod]
		public void ExpressionGetLoosAllTestsTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Where(el=>GenericFilter<int>.Between(el.Score, 0, 4)).Select(el=>el.Test);

			Assert.IsTrue(filtered.Count() == 2);
		}

		[TestMethod]
		public void ExpressionGetSuccessAllTestsTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Where(el => GenericFilter<int>.Between(el.Score, 4, 6)).Select(el => el.Name);

			Assert.IsTrue(filtered.Count() == 2);
		}

		[TestMethod]
		public void ExpressionGetNamesStudentsTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Select(el=>el.Name);

			Assert.IsTrue(filtered.Distinct().Count() == students.Count());
		}

		[TestMethod]
		public void ExpressionGroupByNameStudentTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.GroupBy(el => el.Name);
			var studentsCount = filtered.SelectMany(el => el, (el, testEl) => new {Name = el.Key, Count = el.Count()}).ToList();

			Assert.IsTrue(studentsCount.All(el=>el.Count == 1));
		}

		[TestMethod]
		public void ExpressionGetLongNameTestsTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);
			var filtered = studentsTree.First(el => el.Name.Count() == studentsTree.Max(studentEl => studentEl.Name.Length)).Name;

			Assert.AreEqual(filtered.Length, 24);
		}

		[TestMethod]
		public void ExpressionGetShortNameTestsTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
				new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Filip j. Fry", "Dog test", DateTime.Now.Date, 2),
				new StudentTestInfoBase<int>("Filip j. Fry", "Bending test", DateTime.Now.Date, 1),
			 new StudentTestInfoBase<int>("Filip j. Fry", "20th test", DateTime.Now.Date, 2),
			 new StudentTestInfoBase<int>("Guenter", "20th test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};

			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);
			var filtered = studentsTree.First(el => el.Name.Count() == studentsTree.Min(studentEl => studentEl.Name.Length)).Name;

			Assert.AreEqual(filtered.Length, 7);
		}

		[TestMethod]
		public void ExpressionGetStudentsLeastOneLooseTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
			new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Filip j. Fry", "Dog test", DateTime.Now.Date, 2),
				new StudentTestInfoBase<int>("Filip j. Fry", "Bending test", DateTime.Now.Date, 1),
			 new StudentTestInfoBase<int>("Filip j. Fry", "20th test", DateTime.Now.Date, 2),
			 new StudentTestInfoBase<int>("Guenter", "20th test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Where(el => el.Score == 2).Select(el=>el.Name).Distinct();

			Assert.AreEqual(filtered.Count(), 1);
		}

		[TestMethod]
		public void ExpressionGetThreeStudentsVerySuccessTreeTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
		new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Filip j. Fry", "Dog test", DateTime.Now.Date, 2),
				new StudentTestInfoBase<int>("Filip j. Fry", "Bending test", DateTime.Now.Date, 1),
			 new StudentTestInfoBase<int>("Filip j. Fry", "20th test", DateTime.Now.Date, 2),
			 new StudentTestInfoBase<int>("Guenter", "20th test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = studentsTree.Where(el => el.Score > 2).Take(3);

			Assert.AreEqual(filtered.Count(), 3);
			Assert.IsTrue(filtered.All(el=>el.Score > 2));
		}


		[TestMethod]
		public void ExpressionGetStudentsLeastOneLooseListTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
			new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Filip j. Fry", "Dog test", DateTime.Now.Date, 2),
				new StudentTestInfoBase<int>("Filip j. Fry", "Bending test", DateTime.Now.Date, 1),
			 new StudentTestInfoBase<int>("Filip j. Fry", "20th test", DateTime.Now.Date, 2),
			 new StudentTestInfoBase<int>("Guenter", "20th test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};

			var filtered = students.Where(el => el.Score == 2).Select(el => el.Name).Distinct().ToList();

			Assert.AreEqual(filtered.Count(), 1);
		}

		[TestMethod]
		public void ExpressionGetThreeStudentsVerySuccessListTest()
		{
			var students = new List<StudentTestInfoBase<int>>
			{
		new StudentTestInfoBase<int>("Hubert J. Farnsworth", "[<Platform>]Mathematics of quantum neutrino fields[<300>]", DateTime.Now.Date, 6),
				new StudentTestInfoBase<int>("Bender Bending Rodriguez", "Bending test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Cubert Farnsworth", "Cloning", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Filip j. Fry", "Mathematics of quantum neutrino fields", DateTime.Now.Date, 1),
				new StudentTestInfoBase<int>("Filip j. Fry", "Dog test", DateTime.Now.Date, 2),
				new StudentTestInfoBase<int>("Filip j. Fry", "Bending test", DateTime.Now.Date, 1),
			 new StudentTestInfoBase<int>("Filip j. Fry", "20th test", DateTime.Now.Date, 2),
			 new StudentTestInfoBase<int>("Guenter", "20th test", DateTime.Now.Date, 5),
				new StudentTestInfoBase<int>("Seymour", "Dog test", DateTime.Now.Date, 3)
			};
			var studentsTree = new Tree<StudentTestInfoBase<int>>(students);

			var filtered = students.Where(el => el.Score > 2).GroupBy(el => el.Name).Take(3).ToList();

			Assert.AreEqual(filtered.Count(), 3);
			Assert.IsTrue(filtered.All(el=>el.All(stEl=>stEl.Score>2)));
		}
		#endregion

		#endregion

		#region Exception tests

		[TestMethod()]
		[ExpectedException(typeof(ArgumentNullException))]
		public void WhenRemoveFromEmptyTreeThrowException()
		{
			Tree<int> tree = new Tree<int>();

			tree.RemoveNode(4);
		}

		[TestMethod()]
		[ExpectedException(typeof(ArgumentNullException))]
		public void WhenCreateWithNullNodeThrowException()
		{
			Node<int> node = null;
			new Tree<int>(node);
		}

		[TestMethod()]
		[ExpectedException(typeof(ArgumentNullException))]
		public void WhenCreateWithNullNodesListThrowException()
		{
			List<int> nodes = null;
			new Tree<int>(nodes);
		}

		[TestMethod()]
		[ExpectedException(typeof(ArgumentException))]
		public void WhenCreateWithEmptyListThrowException()
		{
			List<string> nodes = new List<string>();
			new Tree<string>(nodes);
		}

		[TestMethod()]
		[ExpectedException(typeof(ArgumentNullException))]
		public void WhenAddNullDataThrowException()
		{
			string node = null;
			Tree<string> tree = new Tree<string>();

			tree.Add(node);
		}

		[TestMethod()]
		[ExpectedException(typeof(ArgumentNullException))]
		public void WhenRemoveNullDataThrowException()
		{
			string node = null;
			Tree<string> tree = new Tree<string>();

			tree.RemoveNode(node);
		}

		[TestMethod()]
		[ExpectedException(typeof(ArgumentNullException))]
		public void WhenGetMaxWithNullRootThrowException()
		{
			string node = null;
			Tree<string> tree = new Tree<string>();

			tree.GetMax();
		}

		[TestMethod()]
		[ExpectedException(typeof(ArgumentNullException))]
		public void WhenGetMinWithNullRootThrowException()
		{
			string node = null;
			Tree<string> tree = new Tree<string>();

			tree.GetMin();
		}

		#endregion
	}
}